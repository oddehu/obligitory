<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            $this->db = new PDO("mysql:host=localhost;dbname=oblig1", "root", "", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
            // Create PDO connection
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {

        $booklist = array();
        $allBooks = "SELECT * FROM Book";
        foreach( $this->db->query($allBooks) as $row) {
            $booklist[] = new Book($row["title"], $row["author"], $row["description"], $row["id"]);
        }
        return $booklist;

        //$this->load->database();
        //$this->db->reconnect();
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        $spesi = $this->db->prepare("SELECT * FROM Book WHERE id = ?");
          if (!(is_numeric($id))) 
        {
            return NULL;
        }
        $spesi->execute(array($id));
        $inf = $spesi->fetch(PDO::FETCH_ASSOC);

            if($inf != "") {
                $book = new Book($inf["title"], $inf["author"], $inf["description"], $inf["id"]);
                return $book;
            }
            else {
                return $book = NULL;
            }
            //$this->load->database();
            //$this->db->reconnect();
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        if(!($book->title === '' || $book->author === '')) {
        $ins = $this->db->prepare("INSERT INTO Book(title, author, description) VALUES(?, ?, ?)");
        if ($book->description === '') {
            $book->description = NULL;
        }
        $ins->execute(array($book -> title, $book -> author, $book -> description));
        }
        else {
            $view = new ErrorView('Incorrect input. Neither title nor authro can be left empty');
            $view->create();
            return false;
        }

        //$this->load->database();
        //$this->db->reconnect();

    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        if (!($book->title === '' || $book->author === '')) {
            $updt = $this->db->prepare("UPDATE Book SET title=?, author=?, description=? WHERE id=?");
            if ($book->description === '') {
                $book->description = NULL;
            }
            $updt -> bindValue(1, $book->title, PDO::PARAM_STR);
            $updt -> bindValue(2, $book->author, PDO::PARAM_STR);
            $updt -> bindValue(3, $book->description, PDO::PARAM_STR);
            $updt -> bindValue(4, $book->id, PDO::PARAM_INT);
            $updt -> execute();

        }
        else {
            $view = new ErrorView('Incorrect input. Neither title nor authro can be left empty');
            $view->create();
        }

        //  $this->load->database();
        //$this->db->reconnect();   
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        $delB = $this->db->prepare("DELETE FROM Book WHERE id=:id");
        $delB -> bindValue(':id', $id, PDO::PARAM_INT);
        $delB -> execute();

        //$this->load->database();
        //$this->db->reconnect();
    }

    public function checkInp($book) 
    {

    }
	
}

?>
